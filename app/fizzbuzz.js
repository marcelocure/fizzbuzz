const isDivisibleBy = (number, divisor) => {
    return number % divisor === 0;
}

module.exports = (number) => {
    if (isDivisibleBy(number, 3) && isDivisibleBy(number, 5)) return 'FizzBuzz';
    if (isDivisibleBy(number, 3)) return 'Fizz';
    if (isDivisibleBy(number, 5)) return 'Buzz';
    return number;
}