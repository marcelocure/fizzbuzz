const fizzbuzz = require('./fizzbuzz');
const ARRAY_LENGTH = 100;

const numbers = Array.from({length: ARRAY_LENGTH}, (v, k) => k+1);

console.log(numbers.map(fizzbuzz));