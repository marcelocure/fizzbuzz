const fizzbuzz = require('../app/fizzbuzz');
const assert = require('assert');

describe("Fiz buzz should return proper message", () => {
    it("Number is divisible by 3 and 5 should return fizzbuzz", () => {
        assert(fizzbuzz(15) === "FizzBuzz");
    });

    it("Number is divisible by 3 and only 3 should return fizz", () => {
        assert(fizzbuzz(3) === "Fizz");
    });

    it("Number is divisible by 5 and only 5 should return fizz", () => {
        assert(fizzbuzz(5) === "Buzz");
    });

    it("Number is not divisible by 3 and 5 should return the number itself", () => {
        assert(fizzbuzz(1) === 1);
    });
});